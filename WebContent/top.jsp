<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./style.css" rel="stylesheet" type="text/css">
        <title>掲示板</title>
        <script>
		function check(){
			if(window.confirm('削除してよろしいですか？')){
				return true;
			}
			else{
				window.alert('キャンセルされました');
				return false;
			}
		}
		</script>
	</head>

	<body>
		<div class="toptitle">
			掲示板
		</div>
		<c:if test="${ not empty authMessage }">
			<div class="authMessage">
				<ul>
					<c:forEach items="${authMessage}" var="message">
						<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="authMessage" scope="session" />
		</c:if>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name"><c:out value="${loginUser.name}" /></div>
			</div>
		</c:if>

		<div class="main-contents">
			<div class="header">
				<a href="newMessage">新規投稿</a>
				<c:if test="${loginUser.departmentId =='1'}">
					<a href="management">ユーザー管理</a>
				</c:if>
				<a href="logout">ログアウト</a>
			</div>

			<form action="./" method="get">
			<div class="dates">
				日にち<input type="date" name="start" id="start" value="${start}">
				~<input type="date" name="end" id="end" value="${end}">
			</div>

			<div class="category">
				カテゴリー<input type="text" name="category" id="category" value="${category}">
				<input type="submit" value="検索">
			</div>
			</form>
					<c:if test="${ not empty errorComment }">
						<div class="errorComment">
							<ul>
								<c:forEach items="${errorComment}" var="comment">
									<li><c:out value="${comment}" />
								</c:forEach>
							</ul>
						</div>
						<c:remove var="errorComment" scope="session" />
					</c:if>
			<div class="messageandcomment">
				<c:forEach items="${messages}" var="message" >
					<div class ="messages">
						<table>
							<div class="account-name">
								<span class="name"><tr>
								<td>名前</td>
								<td><c:out value="${message.name}" /></td>
								</tr></span>
							</div>
							<div class="message">
								<div class="subject"><tr>
								<td>件名</td>
								<td><c:out value="${message.subject}" /></td>
								</tr></div>
								<div class="text"><tr>
								<td>本文</td>
								<td><c:forEach var="str" items="${fn:split(message.text,'
								')}" >
								<c:out value="${str}" /><br>
								</c:forEach></td>
								</tr></div>

								<div class="category"><tr>
								<td>カテゴリー</td>
								<td><c:out value="${message.category}" /></td>
								</tr></div>
								<div class="date"><tr>
								<td colspan="2"><fmt:formatDate value="${message.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
								</tr></div>
							</div>
							<c:if test = "${loginUser.id == message.userId }">
								<form action="messageDelete" onSubmit="return check()" method="post">
									<tr>
									<td colspan="2"><input type="submit" value="削除"></td>
									</tr>
									<input type="hidden" name="message_id" id="message_id" value="${message.id}">
								</form>
							</c:if>
						</table>
					</div>

					<br>
					<div class="comments">
						<table>
							<c:forEach items="${comments}" var="comment">
								<c:if test="${message.id == comment.messageId}">
									<div class="comments">
										<div class="account-name">
											<span class="name"><tr>
											<td>名前</td>
											<td><c:out value="${comment.name}" /></td>
											</tr></span>
										</div>
										<div class="text"><tr>
										<td>コメント</td>
											<td><c:forEach var="str" items="${fn:split(comment.text,'
											')}" ><c:out value="${str}" /><br>
											</c:forEach></td>
											</tr></div>
										<div class="date"><tr>
										<td colspan="2"><fmt:formatDate value="${comment.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /></td></tr></div>
									</div>
									<c:if test = "${loginUser.id == comment.userId }">
										<form action="commentDelete" onSubmit="return check()" method="post">
											<tr><td colspan="2"><input type="submit" value="削除"></td></tr>
											<input type="hidden" name="comment_id" id="comment_id" value="${comment.id}">
										</form>
									</c:if>
									<br>
								</c:if>
							</c:forEach>
						</table>


						<form action="comment" method="post">
							<label for="text">コメント</label>
							<textarea name="text" rows="5" cols="25" ></textarea>
							<br>
							<input type="submit" value="投稿" />
							<input type="hidden" name="message_id" id="message_id" value="${message.id}">
							<br><br>
						</form>
					</div>
				</c:forEach>
			</div>

		</div>
		<div class="copyright"> Copyright(c)TaroKusachi</div>
	</body>
</html>