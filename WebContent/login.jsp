<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./style.css" rel="stylesheet" type="text/css">
        <title>ログイン</title>
    </head>
    <body>

        <div class="main-contents">
			<div class ="title">
			<ul>
				<p>掲示板にログイン</p>
			</ul>
		</div>
			<c:if test="${ not empty errorMessage }">
				<div class="errorMessage">
					<ul>
						<c:forEach items="${errorMessage}" var="message">
							<c:out value="${message}" />
                        </c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessage" scope="session" />
            </c:if>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="login" method="post"><br />
                <label for="loginId">ログインID</label>
                <input name="login_id" id="login_id" value="${loginId}"/><br>
                <c:remove var="loginId" scope="session"/>


                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <input type="hidden" name="is_deleted" id="is_deleted" value="${user.isDeleted}">

                <input type="submit" value="ログイン" /> <br />
            </form>
            <div class="copyright"> Copyright(c)TaroKusachi</div>
        </div>
    </body>
</html>