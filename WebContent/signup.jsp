<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./style.css" rel="stylesheet" type="text/css">
    <title>新規登録</title>
    </head>
    <body>
		<div class = "header">
		    <header>
				<a href="./">トップ</a>
		    	<a href="management">戻る</a>
		    </header>
	    </div>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
            <br />
				<label for="name">名前</label> <input name="name" id="name" value="${sessionName}"/><br/>
				<c:remove var="sessionName" scope="session"/>
            	<label for="loginId">ログインID</label><input name="login_id" id ="login_id" value="${sessionLoginId}"/><br />
            	<c:remove var="sessionLoginId" scope="session"/>
				<label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
				<label for="confirmPassword">パスワード確認用</label> <input name="confirmPassword" type="password" id="confirmPassword" /> <br />
				<label for="branchId">支店</label>
					<select name="branch_id">
						<c:forEach items="${branches}" var="branch" >
							<option value="${branch.id}"<c:if test="${branch.id == sessionBranch}">selected</c:if>>
								<c:out value="${branch.branch}" />
							</option>
						</c:forEach>
					</select><br />
					<c:remove var="sessionBranch" scope="session"/>
				<label for="departmentId">部署または役職</label>
					<select name="department_id">
						<c:forEach items="${departments}" var="department" >
							<option value="${department.id}"<c:if test="${department.id == sessionDepartment}">selected</c:if>>
								<c:out value="${department.department}" />
							</option>
						</c:forEach>
					</select><br />
					<c:remove var="sessionDepartment" scope="session"/>

                <br /> <input type="submit" value="登録" /> <br />
            </form>
            <div class="copyright">Copyright(c)TaroKusachi</div>
        </div>
    </body>
</html>