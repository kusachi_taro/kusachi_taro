<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./style.css" rel="stylesheet" type="text/css">
        <title>ユーザー管理</title>
        <script>
			function check(){
				if(window.confirm('停止してよろしいですか？')){
					return true;
				}
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
			function check1(){
				if(window.confirm('復活してよろしいですか？')){
					return true;
				}
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>
	</head>

	<body>
		<div class="managementtitle">
			ユーザー管理
		</div>
		<div class = "header">
			<header>
	    			<a href="./">トップ</a>
					<a href="./signup">新規登録</a>
	    	</header>
    	</div>
		<c:if test="${ not empty errorMessage }">
			<div class="errorMessage">
				<ul>
					<c:forEach items="${errorMessage}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessage" scope="session" />
		</c:if>

		<div class="main-contents">
			<div class="users">
				<c:forEach items="${users}" var="user" >
					<table border="1">
						<div class="name"><tr><td>名前</td><td><c:out value="${user.name}" /></td></tr></div>
						<div class="loginId"><tr><td>ログインID</td><td><c:out value="${user.loginId}" /></td></tr></div>
						<c:forEach items="${branches}" var="branch" >
							<c:if test="${user.branchId == branch.id}">
								<div class="branchId"><tr><td>支店</td><td><c:out value="${branch.branch}" /></td></tr></div>
							</c:if>
						</c:forEach>
						<c:forEach items="${departments}" var="department" >
							<c:if test="${department.id == user.departmentId}">
								<div class="departmentId"><tr><td>部署/役職</td><td><c:out value="${department.department}" /></td></tr></div>
							</c:if>
						</c:forEach>

							<tr><td colspan="2"><form action="setting" method="post">
								<a href= "setting?id=${user.id}">編集</a>
							</form></td></tr>

							<c:if test="${user.id !=loginUser.id }">
								<c:if test="${user.isDeleted == 0}">
									<tr><td colspan="2"><form action="switch" method="post" onSubmit="return check()">
										<input type="submit" value="停止">
										<input type="hidden" name="id" id="id" value="${user.id}">
										<input type="hidden" name="is_deleted" id="is_deleted" value="${user.isDeleted}">
									</form></td></tr>
								</c:if>


								<c:if test="${user.isDeleted == 1}">
									<tr><td colspan="2"><form action="switch" method="post" onSubmit="return check1()">
										<input type="submit" value="復活">
										<input type="hidden" name="id" id="id" value="${user.id}">
										<input type="hidden" name="is_deleted" id="is_deleted" value="${user.isDeleted}">
									</form></td></tr>
								</c:if>
							</c:if>
						<br>
					</table>
				</c:forEach>
			</div>

		</div>
		<div class="copyright"> Copyright(c)TaroKusachi</div>
	</body>
</html>