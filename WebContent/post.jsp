<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./style.css" rel="stylesheet" type="text/css">
	<title>新規投稿</title>
	</head>
	<body>
		<div class = "header">
			<header>
	    		<a href="./">トップ</a>
	    	</header>
    	</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							 <c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			<c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="newMessage" method="post">
            <br /> <label for="subject">件名</label> <input name="subject" id="subject" value="${sessionSubject}"/>
            	<c:remove var="sessionSubject" scope="session" />
            <br /> <label for="text">投稿 <textarea name="text" rows="10" cols="50" >${sessionText}</textarea></label>
            	<c:remove var="sessionText" scope="session" />
            <br /> <label for="category">カテゴリー</label> <input name="category" id="category" value="${sessionCategory}"/>
				<c:remove var="sessionCategory" scope="session" />

                <br /> <input type="submit" value="投稿" /> <br />
            </form>
            <div class="copyright">Copyright(c)TaroKusachi</div>
        </div>
    </body>
</html>