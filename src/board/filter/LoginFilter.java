package board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;

@WebFilter(urlPatterns = {"/*"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

	    HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpSession session = ((HttpServletRequest) request).getSession(false);

		String path= httpReq.getRequestURI();
		if(path.endsWith(".css")){
	        chain.doFilter(request,response);
	        return;
	      }

        String servletPath =((HttpServletRequest) request).getServletPath();
        if(servletPath.equals("/login")){
			chain.doFilter(request, response);
        	return;
        }

        User user = (User)session.getAttribute("loginUser");

		if (user == null) {
            List<String> message = new ArrayList<String>();
        	message.add("ログインしてください");
        	session.setAttribute("errorMessage", message);
	        ((HttpServletResponse)response).sendRedirect("login");
	        return;
		}
		else{
			chain.doFilter(request, response);
			return;
		}

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}