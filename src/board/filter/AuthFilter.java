package board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;

@WebFilter(urlPatterns = {"/*"})
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest) request).getSession();

        String servletPath =((HttpServletRequest) request).getServletPath();
        if(!(servletPath.equals("/management") || servletPath.equals("/setting") || servletPath.equals("/signup"))){
			chain.doFilter(request, response);
        	return;
        }

        User user = (User)session.getAttribute("loginUser");

        if(user != null){
	        String departmentId = user.getDepartmentId();

	        if(! user.getDepartmentId().equals("1")){
	            List<String> message = new ArrayList<String>();
	        	message.add("権限がありません");
	        	session.setAttribute("authMessage", message);

		        ((HttpServletResponse)response).sendRedirect("./");
		        return;
	        }

			if (!(departmentId.equals("1"))) {
		        ((HttpServletResponse)response).sendRedirect("./");
		        return;
			}
			else{
				chain.doFilter(request, response);
				return;
			}
        }

        chain.doFilter(request, response);
        return;

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}