package board.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int userId;
    private String subject;
    private String text;
    private String category;
    private Date createdAt;
    private Date updatedAt;

    public int getId(){
    	return id;
    }
    public int getUserId(){
    	return userId;
    }
    public String getSubject(){
    	return subject;
    }
    public String getText(){
    	return text;
    }
    public String getCategory(){
    	return category;
    }
    public Date getCreatedAt(){
    	return createdAt;
    }
    public Date getUpdatedAt(){
    	return updatedAt;
    }
    public void setId(int id){
    	this.id = id;
    }
    public void setUserId(int userId){
    	this.userId = userId;
    }
    public void setSubject(String subject){
    	this.subject = subject;
    }
    public void setText(String text){
    	this.text = text;
    }
    public void setCategory(String category){
    	this.category = category;
    }
    public void setCreatedAt(Date createdAt){
    	this.createdAt = createdAt;
    }
    public void setUpdatedAt(Date updatedAt){
    	this.updatedAt = updatedAt;
    }
}