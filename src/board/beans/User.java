package board.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginId;
    private String name;
    private String password;
    private String confirmPassword;
    private String branchId;
    private String departmentId;
    private String branch;
    private String department;
    private Date createdAt;
    private Date updatedAt;
    private int isDeleted;

    public int getId(){
    	return id;
    }
    public String getLoginId(){
    	return loginId;
    }
    public String getName(){
    	return name;
    }
	public String getPassword() {
		return password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public String getBranchId() {
		return branchId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public String getBranch() {
		return branch;
	}
	public String getDepartment() {
		return department;
	}
	public Date getCreatedAt(){
		return createdAt;
	}
	public Date getUpdatedAt(){
		return updatedAt;
	}
    public int getIsDeleted(){
    	return isDeleted;
    }
	public void setId(int id){
		this.id = id;
	}
	public void setLoginId(String loginId){
		this.loginId = loginId;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setConfirmPassword(String confirmPassword){
		this.confirmPassword = confirmPassword;
	}
	public void setBranchId(String branchId){
		this.branchId = branchId;
	}
	public void setDepartmentId(String departmentId){
		this.departmentId = departmentId;
	}
	public void setBranch(String branch){
		this.branch = branch;
	}
	public void setDepartment(String department){
		this.department = department;
	}
	public void setCreatedAt(Date createdAt){
		this.createdAt = createdAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public void setIsDeleted(int isDeleted){
		this.isDeleted = isDeleted;
	}
}