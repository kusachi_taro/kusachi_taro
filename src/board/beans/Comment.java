package board.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginId;
    private String messageId;
    private String name;
    private int userId;
    private String subject;
    private String text;
    private String category;
    private Date createdAt;

    public int getId(){
    	return id;
    }
    public String getLoginId(){
    	return loginId;
    }
    public String getName(){
    	return name;
    }
    public int getUserId(){
    	return userId;
    }
    public String getMessageId(){
    	return messageId;
    }
    public String getSubject(){
    	return subject;
    }
    public String getText(){
    	return text;
    }
    public String getCategory(){
    	return category;
    }
    public Date getCreatedAt(){
    	return createdAt;
    }
    public void setId(int id){
    	this.id = id;
    }
	public void setLoginId(String loginId){
		this.loginId = loginId;
	}
	public void setName(String name){
		this.name = name;
	}
    public void setUserId(int userId){
    	this.userId = userId;
    }
    public void setMessageId(String messageId){
    	this.messageId = messageId;
    }
    public void setSubject(String subject){
    	this.subject = subject;
    }
    public void setText(String text){
    	this.text = text;
    }
    public void setCategory(String category){
    	this.category = category;
    }
    public void setCreatedAt(Date createdAt){
    	this.createdAt = createdAt;
    }
}