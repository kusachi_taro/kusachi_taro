package board.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private int userId;
    private int messageId;
    private String text;
    private Date createdAt;
    private Date updatedAt;

    public int getId(){
    	return id;
    }
    public String getName(){
    	return name;
    }
    public int getUserId(){
    	return userId;
    }
    public int getMessageId(){
    	return messageId;
    }
    public String getText(){
    	return text;
    }
    public Date getCreatedAt(){
    	return createdAt;
    }
    public Date getUpdatedAt(){
    	return updatedAt;
    }
    public void setId(int id){
    	this.id = id;
    }
    public void setName(String name){
    	this.name = name;
    }
    public void setUserId(int userId){
    	this.userId = userId;
    }
    public void setMessageId(int messageId){
    	this.messageId = messageId;
    }
    public void setText(String text){
    	this.text = text;
    }
    public void setCreatedAt(Date createdAt){
    	this.createdAt = createdAt;
    }
    public void setUpdatedAt(Date updatedAt){
    	this.updatedAt = updatedAt;
    }
}