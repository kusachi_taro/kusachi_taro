package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.UserMessage;
import board.dao.MessageDao;
import board.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String startDate, String endDate, String category) throws UnsupportedEncodingException {

        Connection connection = null;
        try {
            connection = getConnection();
            UserMessageDao messageDao = new UserMessageDao();

			Date date = new Date();
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDate = sdFormat.format(date);

			if(StringUtils.isEmpty(startDate) && StringUtils.isEmpty(endDate)){
				startDate = "2018-01-01 00:00:00";
				endDate = currentDate;
			}
			else if(StringUtils.isEmpty(startDate)){
				startDate = "2018-01-01 00:00:00";
			}
			else if(StringUtils.isEmpty(endDate)){
				endDate = currentDate;
			}

			if(StringUtils.isEmpty(category)){
				category = null;
			}
        	List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, startDate, endDate, category);
            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}