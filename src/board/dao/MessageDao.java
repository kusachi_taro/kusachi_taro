package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.beans.Message;
import board.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append(" ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    	public void delete(Connection connection, Message message) {

            PreparedStatement ps = null;
            try {
                StringBuilder sql = new StringBuilder();
                sql.append("DELETE FROM messages WHERE ");
                sql.append("id = ");
                sql.append("?");

                ps = connection.prepareStatement(sql.toString());

                ps.setInt(1, message.getId());

                ps.executeUpdate();
            }
            catch (SQLException e) {
            	throw new SQLRuntimeException(e);
            } finally {
            	close(ps);
            }

    }

}