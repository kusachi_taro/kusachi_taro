package board.dao;

import static board.utils.CloseableUtil.*;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserMessage;
import board.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String startDate, String endDate, String category) throws UnsupportedEncodingException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_at as created_at ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_at BETWEEN ? ");
            sql.append("AND ? ");
            if(category != null){
            	sql.append("AND category like ? ");
            }
            sql.append("ORDER BY created_at DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate);
            ps.setString(2, endDate + " 23:59:59");
            if(category != null){
            	ps.setString(3, "%" + category + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserMessage message = new UserMessage();
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreatedAt(createdAt);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}