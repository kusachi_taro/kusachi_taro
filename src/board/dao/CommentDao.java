package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.beans.Comment;
import board.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", message_id");
            sql.append(", text");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append(" ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUserId());
            ps.setInt(2, Integer.parseInt(comment.getMessageId()));
            ps.setString(3, comment.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void delete(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments WHERE ");
            sql.append("id = ");
            sql.append("?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getId());

            ps.executeUpdate();
        }
        catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }

	}
}