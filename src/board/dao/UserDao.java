package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.User;
import board.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(", is_deleted");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", CURRENT_TIMESTAMP");
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getBranchId());
            ps.setString(5, user.getDepartmentId());
            ps.setInt(6, user.getIsDeleted());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String branchId = rs.getString("branch_id");
                String departmentId = rs.getString("department_id");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");
                int isDeleted = rs.getInt("is_deleted");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setName(name);
                user.setPassword(password);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setCreatedAt(createdAt);
                user.setUpdatedAt(updatedAt);
                user.setIsDeleted(isDeleted);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    private User toUser(ResultSet rs) throws SQLException {

        User user = new User();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String branchId = rs.getString("branch_id");
                String departmentId = rs.getString("department_id");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");
                int isDeleted = rs.getInt("is_deleted");

                user.setId(id);
                user.setLoginId(loginId);
                user.setName(name);
                user.setPassword(password);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setCreatedAt(createdAt);
                user.setUpdatedAt(updatedAt);
                user.setIsDeleted(isDeleted);

            }
            return user;
        } finally {
            close(rs);
        }
    }
    private List<User> toBranchList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String branch = rs.getString("name");

                User user = new User();
                user.setId(id);
                user.setBranch(branch);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    private List<User> toDepartmentList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String department = rs.getString("name");

                User user = new User();
                user.setId(id);
                user.setDepartment(department);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            }else {
                return userList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getBranches(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> branchList = toBranchList(rs);
            if (branchList.isEmpty() == true) {
                return null;
            }else {
                return branchList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getDepartments(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM departments";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> departmentList = toDepartmentList(rs);
            if (departmentList.isEmpty() == true) {
                return null;
            }else {
                return departmentList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            if(user.getPassword()!=null){
            	sql.append(", password = ?");
            }
            if(user.getBranchId()!=null){
            	sql.append(", branch_id = ?");
            	sql.append(", department_id = ?");
            }
            sql.append(", updated_at = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            if(user.getPassword()!=null && user.getBranchId()!=null){
            	ps.setString(3, user.getPassword());
            	ps.setString(4, user.getBranchId());
            	ps.setString(5, user.getDepartmentId());
            	ps.setInt(6, user.getId());
            }
            else if(user.getPassword()!=null && user.getBranchId()==null){
            	ps.setString(3, user.getPassword());
            	ps.setInt(4, user.getId());
            }
            else if(user.getPassword()==null && user.getBranchId()!=null){
            	ps.setString(3, user.getBranchId());
            	ps.setString(4, user.getDepartmentId());
            	ps.setInt(5, user.getId());
            }
            else{
            	ps.setInt(3, user.getId());
            }

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    public void switching(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  is_deleted = ?");
            sql.append(", updated_at = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user.getIsDeleted());
            ps.setInt(2, user.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    public User duplicateUser(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users where login_id = ?";

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, user.getLoginId());

            ResultSet rs = ps.executeQuery();
            User duplicateUser = (User) toUser(rs);
            if (duplicateUser.getLoginId() == null) {
                return null;
            }else {
                return duplicateUser;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}