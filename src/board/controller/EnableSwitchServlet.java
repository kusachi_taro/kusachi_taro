package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/switch" })
public class EnableSwitchServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
        if(Integer.parseInt(request.getParameter("is_deleted"))==0){
        	editUser.setIsDeleted(1);
        }
        else{
        	editUser.setIsDeleted(0);
        }
        new UserService().switching(editUser);

        session.setAttribute("editUser", editUser);
        response.sendRedirect("management");

    }

}