package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.UserComment;
import board.beans.UserMessage;
import board.service.CommentService;
import board.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        String startDate = request.getParameter("start");
        String endDate = request.getParameter("end");
        String category = request.getParameter("category");

        List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);
	    request.setAttribute("comments", comments);

        session.setAttribute("start", startDate);
        session.setAttribute("end", endDate);
        session.setAttribute("category", category);

        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

}