package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> branchList = new UserService().getBranches();
        List<User> departmentList = new UserService().getDepartments();

        request.setAttribute("branches", branchList);
        request.setAttribute("departments", departmentList);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        List<User> branchList = new UserService().getBranches();
        List<User> departmentList = new UserService().getDepartments();

        request.setAttribute("branches", branchList);
        request.setAttribute("departments", departmentList);

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setConfirmPassword(request.getParameter("confirmPassword"));
            user.setBranchId(request.getParameter("branch_id"));
            user.setDepartmentId(request.getParameter("department_id"));
            user.setIsDeleted(0);

        	new UserService().register(user);
            response.sendRedirect("management");
            return;
        } else {
            session.setAttribute("sessionName", request.getParameter("name"));
            session.setAttribute("sessionLoginId", request.getParameter("login_id"));
        	session.setAttribute("sessionBranch", request.getParameter("branch_id"));
        	session.setAttribute("sessionDepartment", request.getParameter("department_id"));
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String name = request.getParameter("name");
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String branchId = request.getParameter("branch_id");
        String departmentId = request.getParameter("department_id");
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setLoginId(request.getParameter("login_id"));
        user.setPassword(request.getParameter("password"));
        user.setConfirmPassword(request.getParameter("confirmPassword"));
        user.setBranchId(request.getParameter("branch_id"));
        user.setDepartmentId(request.getParameter("department_id"));
        user.setIsDeleted(0);
        User duplicateUser = new UserService().getUsers(user);

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if(10 < name.length()){
        	messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }
        else{
	        if(20 < loginId.length()){
	        	messages.add("ログインIDは20文字以下で入力してください");
	        }
	        if(6 > loginId.length()){
	        	messages.add("ログインIDは6文字以上で入力してください");
	        }
        }
        if(duplicateUser != null){
        	messages.add("ログインIDが重複しています");
        }
        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }
        else{
	        if(20 < password.length()){
	        	messages.add("パスワードは20文字以下で入力してください");
	        }
	        if(6 > password.length()){
	        	messages.add("パスワードは6文字以上で入力してください");
	        }
        }
        if(!request.getParameter("password").equals(request.getParameter("confirmPassword"))){
        	messages.add("パスワードが一致しません");
        }
        if (StringUtils.isEmpty(branchId) == true) {
            messages.add("支店を入力してください");
        }
        if (StringUtils.isEmpty(departmentId) == true) {
            messages.add("部署または役職を入力してください");
        }
        if(branchId.equals("1") && (departmentId.equals("3")||departmentId.equals("4"))||
        (branchId.equals("2")||branchId.equals("3")||branchId.equals("4")) && (departmentId.equals("1")||departmentId.equals("2"))){
        	messages.add("不正な組み合わせです");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}