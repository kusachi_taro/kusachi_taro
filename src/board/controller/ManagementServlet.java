package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> userList = new UserService().getUsers();
        List<User> branchList = new UserService().getBranches();
        List<User> departmentList = new UserService().getDepartments();

        request.setAttribute("branches", branchList);
        request.setAttribute("departments", departmentList);
        request.setAttribute("users", userList);

        request.getRequestDispatcher("management.jsp").forward(request, response);

    }
}