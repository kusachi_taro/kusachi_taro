package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.Comment;
import board.service.CommentDeleteService;

@WebServlet(urlPatterns = {"/commentDelete"})
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

			HttpSession session = request.getSession();

            Comment comment = new Comment();
            comment.setId(Integer.parseInt(request.getParameter("comment_id")));
            new CommentDeleteService().register(comment);

            session.setAttribute("comment",comment);
            response.sendRedirect("./");

	}

}
