package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

	    User user = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
	    request.setAttribute("editUser", user);

    	if(!request.getParameter("id").matches("\\d+")){
	        HttpSession session = request.getSession();
			session.setAttribute("errorMessage", "不正なURLです");
			response.sendRedirect("management");
			return;
    	}
    	else if(user==null){
	        HttpSession session = request.getSession();
			session.setAttribute("errorMessage", "不正なURLです");
			response.sendRedirect("management");
			return;
    	}

        List<User> branchList = new UserService().getBranches();
        List<User> departmentList = new UserService().getDepartments();

        request.setAttribute("branches", branchList);
        request.setAttribute("departments", departmentList);

        request.getRequestDispatcher("setting.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();

        List<User> branchList = new UserService().getBranches();
        List<User> departmentList = new UserService().getDepartments();

        request.setAttribute("branches", branchList);
        request.setAttribute("departments", departmentList);

        if (isValid(request, messages) == true) {

            User editUser = new User();
            editUser.setId(Integer.parseInt(request.getParameter("id")));
            editUser.setName(request.getParameter("name"));
            editUser.setLoginId(request.getParameter("login_id"));
            if(!request.getParameter("password").isEmpty()){
            	editUser.setPassword(request.getParameter("password"));
            }
            else{
            	editUser.setPassword(editUser.getPassword());
            }
            editUser.setBranchId(request.getParameter("branch_id"));
            editUser.setDepartmentId(request.getParameter("department_id"));

            new UserService().update(editUser);
            session.setAttribute("editUser", editUser);
            response.sendRedirect("management");
            return;
        } else {
            session.setAttribute("sessionName", request.getParameter("name"));
            session.setAttribute("sessionLoginId", request.getParameter("login_id"));
        	session.setAttribute("sessionBranch", request.getParameter("branch_id"));
        	session.setAttribute("sessionDepartment", request.getParameter("department_id"));
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
        	session.setAttribute("id", request.getParameter("id"));
            return;
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String name = request.getParameter("name");
        String loginId = request.getParameter("login_id");

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setLoginId(request.getParameter("login_id"));
        editUser.setBranchId(request.getParameter("branch_id"));
        editUser.setDepartmentId(request.getParameter("department_id"));
        User duplicateUser = new UserService().getUsers(editUser);
        String branchId = request.getParameter("branch_id");
        String departmentId = request.getParameter("department_id");

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if(10 < name.length()){
        	messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }
        if(20 < loginId.length()){
        	messages.add("ログインIDは20文字以下で入力してください");
        }
        if(6 > loginId.length() && StringUtils.isBlank(loginId) == false){
        	messages.add("ログインIDは6文字以上で入力してください");
        }
	    User user = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
	    if(duplicateUser != null && !request.getParameter("login_id").equals(user.getLoginId())){
	        messages.add("ログインIDが重複しています");
        }
	    if(!StringUtils.isEmpty(request.getParameter("password"))){
	    	String password = request.getParameter("password");
			    if(20 < password.length()){
			    	messages.add("パスワードは20文字以下で入力してください");
			    }
			    if(6 > password.length()){
			    	messages.add("パスワードは6文字以上で入力してください");
			    }
	    }
        if(! request.getParameter("password").equals(request.getParameter("confirmPassword"))){
        	messages.add("パスワードが一致しません");
        }
        if(StringUtils.isEmpty(branchId) == false && StringUtils.isEmpty(departmentId) == false){
	        if(branchId.equals("1") && (departmentId.equals("3")||departmentId.equals("4"))||
	        (branchId.equals("2")||branchId.equals("3")||branchId.equals("4")) && (departmentId.equals("1")||departmentId.equals("2"))){
	        	messages.add("不正な組み合わせです");
	        }
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}